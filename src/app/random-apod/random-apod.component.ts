import { Component, OnInit } from '@angular/core';
import { Apod } from '../apod';
import { ApodService } from '../apod.service';

@Component({
  selector: 'app-random-apod',
  templateUrl: './random-apod.component.html',
  styleUrls: ['./random-apod.component.css']
})
export class RandomApodComponent implements OnInit {

  constructor(private apodService: ApodService) { }

  randomApod: Apod | undefined

  ngOnInit(): void {
    this.getRandomPicture()
  }

  getRandomPicture(): void {
    this.apodService.getApods(1)
      .subscribe(apod => this.randomApod = apod[0])
  }

}
