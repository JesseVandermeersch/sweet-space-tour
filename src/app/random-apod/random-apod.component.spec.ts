import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomApodComponent } from './random-apod.component';

describe('RandomApodComponent', () => {
  let component: RandomApodComponent;
  let fixture: ComponentFixture<RandomApodComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RandomApodComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomApodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
