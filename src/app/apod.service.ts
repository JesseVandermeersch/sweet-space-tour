import { Injectable } from '@angular/core';
import { Apod } from './apod';
import { Observable, of } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ApodService {

  constructor(
    private http: HttpClient,
  ) { }

  private apiUrl = environment.apiUrl;
  public requestedPicture: Apod | undefined;

  getApods(count:number = 6): Observable<Apod[]> {
    return this.http.get<Apod[]>(`${this.apiUrl}&thumbs=true&count=${count}`)
  }

  getApod(date: string | null): Observable<Apod> {
    const url = `${this.apiUrl}&date=${date}`
    return this.http.get<Apod>(url)
      .pipe (
        tap(_ => console.log(`fetched apod from ${date}`)),
        catchError(this.handleError<Apod>(`getApod date: ${date}`))
      );
  }

  handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      console.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    }
  }
}
