import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ApodsComponent } from './apods/apods.component';
import { ApodDetailComponent } from './apod-detail/apod-detail.component';
import { ApodByDateComponent } from './apod-by-date/apod-by-date.component';
import { RandomApodComponent } from './random-apod/random-apod.component';

const routes: Routes = [
  { path: '', redirectTo: '/pictures', pathMatch: 'full' },
  { path: 'pictures', component: ApodsComponent },
  { path: 'pictures/:date', component: ApodDetailComponent },
  { path: 'apod-by-date', component: ApodByDateComponent },
  { path: 'random', component: RandomApodComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
