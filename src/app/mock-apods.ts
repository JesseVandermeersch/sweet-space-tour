import { Apod } from "./apod";

export const APODS: Apod[] = [
  {
    title: "Camelopardalids and ISS",
    url: "https://apod.nasa.gov/apod/image/1405/ISSCamelopardalidsLMalcolmPark950.jpg",
    mediaType: "image",
    explanation: "From a camp on the northern shores of the Great Lake Erie, three short bright meteor streaks were captured in this composited night skyscape. Recorded over the early morning hours of May 24, the meteors are elusive Camelopardalids. Their trails point back to the meteor shower's radiant near Polaris, in the large but faint constellation Camelopardalis the camel leopard, or in modern terms the Giraffe. While a few meteors did appear, the shower was not an active one as the Earth crossed through the predicted debris trail of periodic comet 209P/LINEAR. Of course, the long bright streak in the image did appear as predicted. Early on May 24, the International Space Station made a bright passage through northern skies.",
    date: "2014-05-25",
  },
  {
    title: "The Surface of Titan",
    url: "https://apod.nasa.gov/apod/image/0008/titan_cfht.jpg",
    mediaType: "image",
    explanation: "If sailing the hydrocarbon seas of Titan, beware of gasoline rain.  Such might be a travel advisory issued one future day for adventurers visiting Titan, the largest moon of Saturn.   New images of Titan's surface were released last week from the Canada-France Hawaii Telescope featuring the finest details yet resolved.  Peering into Titan's thick smog atmosphere with infrared light, complex features interpreted as oceans, glaciers, and rock became visible.  The high-resolution infrared image pictured above was made possible using an unblurring technique called adaptive optics.  The interplanetary probe Cassini will reach Saturn and Titan in 2004 to better explore this unusual world.",
    date: "2000-08-20",
  },
  {
    title: "Hand Drawn Transit",
    url: "https://apod.nasa.gov/apod/image/0611/mercurytransit_seibold_90.jpg",
    mediaType: "image",
    explanation: "The sight of Mercury's tiny round disk drifting slowly across the face of the Sun inspired and entertained many denizens of planet Earth last week. In fact, artist and astronomer Mark Seibold viewed both the 1999 and 2006 transits of the solar system's innermost planet through solar filtered telescopes and composed this rendering of Mercury \"hovering in the photosphere\" near the edge of an enormous solar disk. The original work is a 23 by 17 inch pastel sketch. While the artist's hand is creatively superimposed, Seibold concentrated on offering an impression of Mercury's silhouette, surrounded by shadings reflecting his visual experience that are not easily captured in photographic exposures. Of course, before the age of cameras drawings were more widely used to record telescopic observations of sunspots and planetary transits.",
    date: "2006-11-17",
  },
  {
    title: "X-Ray Triple Jet",
    url: "https://apod.nasa.gov/apod/image/9807/triple_jet.gif",
    mediaType: "image",
    explanation: "Recorded on July 7, 1998, this animation using X-ray images of the Sun shows an amazing event - three nearly simultaneous jets connected with solar active regions. The two frames were taken several hours apart by the Soft X-ray Telescope on board the orbiting Yohkoh observatory. They have a \"negative\" color scheme, the darker colors representing more intense X-rays from the corona and active regions on the solar surface. The pictures clearly show two curving jets of X-ray hot plasma appearing above the solar equator and one below. A sharp vertical stripe near the jet above center is a digital blemish while the overall shift of the image is due to solar rotation. As the Sun is now approaching the active part of its 11 year cycle, numerous single jets have been seen. But the appearance of these three widely separated jets at once is considered an unlikely coincidence and has fueled current speculations about their origins.",
    date: "1999-07-31",
  },
  {
    title: "The View Toward M106",
    url: "https://apod.nasa.gov/apod/image/1601/m106_neyer1024.jpg",
    mediaType: "image",
    explanation: "A big, bright, beautiful spiral, Messier 106 is at the center of this galaxy filled cosmic vista. The two degree wide telescopic field of view looks toward the well-trained constellation Canes Venatici, near the handle of the Big Dipper. Also known as NGC 4258, M106 is about 80,000 light-years across and 23.5 million light-years away, the largest member of the Canes II galaxy group. For a far away galaxy, the distance to M106 is well-known in part because it can be directly measured by tracking this galaxy's remarkable maser, or microwave laser emission. Very rare but naturally occuring, the maser emission is produced by water molecules in molecular clouds orbiting its active galactic nucleus. Another prominent spiral galaxy on the scene, viewed nearly edge-on, is NGC 4217 below and right of M106. The distance to NGC 4217 is much less well-known, estimated to be about 60 million light-years.",
    date: "2016-01-16",
  },
  {
    title: "Sun with Solar Flare",
    url: "https://apod.nasa.gov/apod/image/1304/sdo_20130411-M6flare-orig_900c.jpg",
    mediaType: "image",
    explanation: "This week the Sun gave up its strongest solar flare so far in 2013, accompanied by a coronal mass ejection (CME) headed toward planet Earth. A false-color composite image in extreme ultraviolet light from the Solar Dynamics Observatory captures the moment, recorded on April 11 at 0711 UTC. The flash, a moderate, M6.5 class flare erupting from active region AR 11719, is near the center of the solar disk. Other active regions, areas of intense magnetic fields seen as sunspot groups in visible light, mottle the surface as the solar maximum approaches. Loops and arcs of glowing plasma trace the active regions' magnetic field lines. A massive cloud of energetic, charged particles, the CME will impact the Earth's magnetosphere by this weekend and skywatchers should be on the alert for auroral displays.",
    date: "2013-04-13",
  },

]