export interface Apod {
  title: string,
  url: string,
  mediaType: string,
  date: string,
  explanation: string,
}