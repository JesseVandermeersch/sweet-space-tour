import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApodByDateComponent } from './apod-by-date.component';

describe('ApodByDateComponent', () => {
  let component: ApodByDateComponent;
  let fixture: ComponentFixture<ApodByDateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApodByDateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApodByDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
