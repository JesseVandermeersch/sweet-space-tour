import { Component, OnInit } from '@angular/core';
import { Apod } from '../apod';
import { ApodService } from '../apod.service';

@Component({
  selector: 'app-apod-by-date',
  templateUrl: './apod-by-date.component.html',
  styleUrls: ['./apod-by-date.component.css']
})
export class ApodByDateComponent implements OnInit {

  picturebyDate: Apod | undefined

  constructor(private apodService: ApodService) { }

  ngOnInit(): void {
  }

  getPictureByDate(date: string) {
    this.apodService.getApod(date)
      .subscribe(apod => this.picturebyDate = apod);
  }

}
