import { Component, OnInit } from '@angular/core';
import { Apod } from '../apod';
import { APODS } from '../mock-apods';
import { ApodService } from '../apod.service';

@Component({
  selector: 'app-apods',
  templateUrl: './apods.component.html',
  styleUrls: ['./apods.component.css']
})
export class ApodsComponent implements OnInit {

  constructor(private apodService: ApodService) { }

  apods: Apod[] = [];
  requestedPicture: Apod | undefined;

  ngOnInit(): void {
    this.getApods();
  }

  selectedApod?: Apod;
  onSelect(apod: Apod): void {
    this.selectedApod = apod;
  }

  getApods(): void {
    this.apodService.getApods()
      .subscribe(apods => this.apods = apods);
  }

}
