import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApodsComponent } from './apods/apods.component';
import { ApodDetailComponent } from './apod-detail/apod-detail.component';
import { ApodByDateComponent } from './apod-by-date/apod-by-date.component';
import { RandomApodComponent } from './random-apod/random-apod.component';

@NgModule({
  declarations: [
    AppComponent,
    ApodsComponent,
    ApodDetailComponent,
    ApodByDateComponent,
    RandomApodComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
