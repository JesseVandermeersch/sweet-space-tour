import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Apod } from '../apod';
import { ApodService } from '../apod.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-apod-detail',
  templateUrl: './apod-detail.component.html',
  styleUrls: ['./apod-detail.component.css']
})
export class ApodDetailComponent implements OnInit {

  apod: Apod | undefined;

  constructor(
    private route: ActivatedRoute,
    private apodService: ApodService,
    private location: Location
  ) {}

  ngOnInit(): void {
    this.getHero();
  }

  getHero(): void {
    const date = this.route.snapshot.paramMap.get('date');
    this.apodService.getApod(date)
      .subscribe(apod => this.apod = apod);
  }

  goBack(): void {
    this.location.back();
  }

}
